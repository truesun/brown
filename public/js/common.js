var hamSwitch = false;
var windowRW = 767;
var windowWAct, scrollTop;
$(document).ready(function () {
    //漢堡開關
    var $hamburger = $("#hamBtn");
    var $hamCloseBtn = $("#hamCloseBtn");
    $hamburger.on("click", function (e) {
        //== 動畫 ==
        gsap.to("#mbNavBg", { height: '100vh', duration: 0.6, ease: "power1.out" });
        $('.mb-nav-area').show();
        gsap.to(".ani-item", {
            y: 0, duration: 0.4, ease: "power1.out", stagger: {
                // grid: [7, 15],
                // from: "center",
                amount: 0.2
            }
        });
        hamSwitch = true;
        $hamburger.addClass("is-active");
        $hamCloseBtn.addClass("is-active");
        bgFix();
        // if (hamSwitch) {
        //     $hamburger.removeClass("is-active");
        //     // $navItem.css({ 'display': 'flex' });
        //     // $navItem.slideUp();
        //     hamSwitch = false;
        // } else {
        //     $hamburger.addClass("is-active");
        //     // $navItem.css({ 'display': 'none' });
        //     // $navItem.slideDown();
        //     hamSwitch = true;
        // }
    });
    $hamCloseBtn.on("click", function (e) {
        //== 動畫 ==
        $('.mb-nav-sub').hide();
        gsap.to("#mbNavBg", { height: '0', duration: 0.6, ease: "power1.in" });
        gsap.to(".ani-item", {
            y: 30, duration: 0.4, ease: "power1.out",
            stagger: {
                // grid: [7, 15],
                from: "end",
                amount: 0.2
            },
            onComplete() {
                console.log('whole tween done');
                $('.mb-nav-area').hide();
                // gsap.to(this.targets(), {
                //     x: 60, background: 'red', duration: .2
                // })
            },
        });
        hamSwitch = false;
        $hamburger.removeClass("is-active");
        $hamCloseBtn.removeClass("is-active");
        bgUnFix();
    });

    //=== nav hover bg ===
    // var windowRW = 767;
    var windowWAct;
    scrollTop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
    var bannerAreaH = $('#bannerArea').outerHeight();
    //1:136 , 2:170 , 3:68 , 4:68
    var hoverNavSunList = [{ 'navSubId': '1', 'height': 170 }, { 'navSubId': '2', 'height': 170 }, { 'navSubId': '3', 'height': 170 }, { 'navSubId': '4', 'height': 170 }];
    var hoverId;
    $.each(hoverNavSunList, function (ind, val) {
        $('#navTit' + val.navSubId).on('mouseenter', function () {
            windowWAct = $(window).width();
            if (windowWAct >= windowRW) {
                scrollTop < bannerAreaH && $('#navArea').addClass('on');
                var navH = $('#navArea').outerHeight();
                var totalH = navH + val.height;
                // console.log(totalH, val.height);
                $('#navAreaBg').css('height', totalH);
                $('.nav-area').css({ 'box-shadow': 'none' });
                //== nav mouse use ==
                $(this).addClass('act');
                hoverId = val.navSubId;
                $('#navSub' + hoverId).addClass('act');
                $('#navSocialBox').addClass('act');//= social 的顯示 =
                $('#navMouseArea').addClass('act');//= 次選單hover的時候避免側邊社群跳動所以給固定高 =
            }
        });
        $('#navTit' + val.navSubId).on('mouseleave', function () {
            windowWAct = $(window).width();
            var bannerVideoIsShow = $('#bannerVideoBox').is(':visible');
            if (windowWAct >= windowRW) {
                if (bannerVideoIsShow) {
                    //== 在banner範圍內執行 ==
                    scrollTop < bannerAreaH && $('#navArea').removeClass('on');
                }
                $('#navAreaBg').removeAttr('style');
                $('.nav-area').removeAttr('style');
                //== nav mouse use ==
                $(this).removeClass('act');
                $('.nav-mouse-link-box').removeClass('act');
                $('#navSocialBox').removeClass('act');
                $('#navMouseArea').removeClass('act');
            }
        });
    });
    //=== nav mouseHover ===
    $('#navMouseArea').on('mouseenter', function () {
        windowWAct = $(window).width();
        if (windowWAct >= windowRW) {
            scrollTop < bannerAreaH && $('#navArea').addClass('on');
            var navH = $('#navArea').outerHeight();
            console.log(hoverNavSunList[hoverId - 1]);
            var totalH = navH + hoverNavSunList[hoverId - 1].height;
            // console.log(totalH, val.height);
            $('#navAreaBg').css('height', totalH);
            $('.nav-area').css({ 'box-shadow': 'none' });
            //====
            $('#navTit' + hoverId).addClass('act');
            $('#navSub' + hoverId).addClass('act');
            $('#navSocialBox').addClass('act');
            // hoverId = val.navSubId;
            $('#navMouseArea').addClass('act');
            console.log(hoverId);
        }
    });
    $('#navMouseArea').on('mouseleave', function () {
        windowWAct = $(window).width();
        var bannerVideoIsShow = $('#bannerVideoBox').is(':visible');
        if (windowWAct >= windowRW) {
            if (bannerVideoIsShow) {
                //== 在banner範圍內執行 ==
                scrollTop < bannerAreaH && $('#navArea').removeClass('on');
            }
            $('#navAreaBg').removeAttr('style');
            $('.nav-area').removeAttr('style');
            //====
            $('#navTit' + hoverId).removeClass('act');
            $('.nav-mouse-link-box').removeClass('act');
            $('#navSocialBox').removeClass('act');
            $('#navMouseArea').removeClass('act');
        }
    });

    //=== gotop ===
    $("#goTopBtn").on('click', function () {
        $('html,body').animate({ scrollTop: 0 }, 'slow');
    });

    //=== mb nav click ===
    $('.mb-nav-tit-box').on('click', function () {
        var $subCont = $(this).next();
        var subShow = $subCont.is(':visible');
        $('.mb-nav-sub').slideUp();
        if (subShow) {
            $subCont.slideUp();
        } else {
            $subCont.slideDown();
        }
    });

    //== footer手機上點擊功能 ==
    var windowWAct = $(window).width();
    if (windowWAct <= windowRW) {
        //=== 如果是手機的時候 footer要加點擊 ===
        $('.footer-link').find('.fc').addClass('fclick');
    }

    $('.fclick').on('click', function () {
        var $cont = $(this).next();
        var contShow = $cont.is(':visible');
        $('.fclick').next().slideUp();
        $('.fclick').find('.icon').removeClass('on');
        if (contShow) {
            $cont.slideUp();
        } else {
            $cont.slideDown();
            $(this).find('.icon').addClass('on');
        }
    });
});

$(window).resize(function () {
    //=== mb nav狀態 ===
    // var windowRW = 767;
    var windowWAct = $(window).width();
    if (windowWAct > windowRW) {
        //== nav ==
        $('.ani-item,.mb-nav-area,#mbNavBg').removeAttr('style');
        hamSwitch = false;
        $("#hamBtn,#hamCloseBtn").removeClass("is-active");
        //== footer ==
        $('.footer-link').find('.fc').removeClass('fclick');
        $('.footer-link').find('.link').removeAttr('style');
        $('.footer-link').find('.icon').removeClass('on');
    } else {
        //== footer ==
        $('.footer-link').find('.fc').addClass('fclick');
    }
});


$(window).scroll(function () {
    scrollTop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
    //=== nav change ===
    var navH = $('#navArea').outerHeight();
    var bannerAreaH = $('#bannerArea').outerHeight();
    var changeH = bannerAreaH - navH;
    var navisHover = $('#navTit2').is(':hover');
    var bannerVideoIsShow = $('#bannerVideoBox').is(':visible');
    // var windowRW = 767;
    var windowWAct;
    windowWAct = $(window).width();
    if (windowWAct > windowRW) {//電腦寬度時才作用
        if (!navisHover && bannerVideoIsShow) {
            scrollTop > changeH ? $('#navArea').addClass('on') : $('#navArea').removeClass('on');
        }
    }

    //=== gotop ===
    var hasFooter = $('.footer-area').is(':visible');
    if (hasFooter) {
        var footerH = $('.footer-area').offset().top;
    } else {
        var footerH = 0;
    }
    var windowH = $(window).height();
    var footerBotH = footerH - windowH;
    // if (scrollTop > 500) {
    //     $("#goTopBtn").addClass('on');
    // } else {
    //     $("#goTopBtn").removeClass('on');
    // }
    if (scrollTop > 300) {
        if (scrollTop > footerBotH) {
            $("#socialSide").removeClass('on');
        } else {
            $("#socialSide").addClass('on');
        }
    } else {
        $("#socialSide").removeClass('on');
    }

});

var scTop;
function bgFix() {
    scTop = $(document).scrollTop();
    $('body').css({ 'top': -scTop, 'position': 'fixed', 'width': '100%' })
}
function bgUnFix() {
    $('body').removeAttr('style');
    $('html,body').animate({ scrollTop: scTop }, 0);
}
// function bgFix() {
//     $('body').css({ 'overflow': 'hidden' });
// }
// function bgUnFix() {
//     $('body').removeAttr('style');
// }